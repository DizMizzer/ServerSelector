package nl.dizmizzer.ss;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

/**
 * Created by DizMizzer.
 * Users don't have permission to release
 * the code unless stated by the Developer.
 * You are allowed to copy the source code
 * and edit it in any way, but not distribute
 * it. If you want to distribute addons,
 * please use the API. If you can't access
 * a certain thing in the API, please contact
 * the developer in contact.txt.
 */
public class ServerSelector extends JavaPlugin implements PluginMessageListener {

    public void onEnable() {
        getServer().getPluginManager().registerEvents(new EventList(this), this);
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);
    }

    public void onDisable() {

    }

    @Override
    public void onPluginMessageReceived(String s, Player player, byte[] bytes) {

    }
}
